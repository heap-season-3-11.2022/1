import java.util.Scanner;
public class WorkwithStrings
{
	public static void main(String[] args) {
		System.out.println("Введите строку (разделитель слов - пробел): ");
		Scanner myscan = new Scanner (System.in); //Сканер
		String str1 ="", str2="", output = ""; //Проанализированная строка; анализируемая строка; выводная строка
		String minWord = ""; //Слово с минимальным кол-вом разных символов
		int count = 10000, count_temp = 1, count_only_literal = 0; //Счетчик, с которым будут сравниваться count_temp; 
		// счетчик символов минимального слова; счетчик слов, у которых только латинские буквы
		String oo = new String ("object-oriented"), p = new String ("programming"); //Строки для замены "object-oriented programming" на "OOP"
		boolean oop = false;
		
		do 
		{
		    str1 = str2;
		    str2 = myscan.next();
            
            boolean b0 = true;
            for (int i = 0; i < str2.length()/2; i++) //Палиндром или нет?
            {
                char c1 = str2.charAt(i);
                int k = str2.length();
                char c2 = str2.charAt(k - 1 - i);
                if (c1 != c2)
                {
                    b0 = false;
                    break;
                }
            }
            if (b0 == true)
            {
                System.out.println("Палиндром: " + str2);
            }
            
            b0 = true;
            for (int i = 0; i < str2.length(); i++) //Латинские буквы или не только
            {
                char c = str2.charAt(i);
                b0 = false;
                for (int j = 97; j < 122; j++)
                {
                    char c0 = (char) j;
                    if (c0 == c)
                    {
                        b0 = true;
                    }
                }
                for (int j = 65; j < 91; j++)
                {
                    char c0 = (char) j;
                    if (c0 == c)
                    {
                        b0 = true;
                    }
                }
               if (b0 == false) break;
            }   
            if (b0 == true)
            {
                count_only_literal++;
            }           
            
            count_temp = 1;
            if (str2.equals("&")) //Поиск слова с наименьшим кол-вом различных символов
            {  }
		    else 
		    {
		        for (int i = 1; i < str2.length(); i++) 
		        {
		           b0 = true;
		            for (int j = i - 1; j >= 0; j--)
		        {
		            char c1 = str2.charAt(i);
		            char c2 = str2.charAt(j);
		            if (c1 == c2)
		            {
		                b0 = false;
		            }
		        }
		            if (b0 == true) count_temp++;
		        }
		    
		        if (count_temp < count)
		        {
		        	minWord = str2;
		        	count = count_temp;
		        }
		    }
		    
		    if (str1.equalsIgnoreCase(oo) && str2.equalsIgnoreCase(p)) //Сравнение ООП без учета регистра
		    {
		        output = output + " " + "OOP";
		        oop = true;
		    }
		    else
		    {
		        if (oop == true) //Т.к. обрабатываю код по одному слову, а ООП - два слова, то приходится такую проверку
		        {
		            oop = false;   
		        }
		        else
		        {
		            output = output + " " + str1;
		            
		        }
		    }
		    if (str2.equals("&")) break; //Стоп-слово
		} while(true);
		System.out.println(output);
		System.out.println("     " + minWord);
		System.out.println("     " + count_only_literal);
	}
}


