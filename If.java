import java.util.Scanner;

public class If
{
	public static void main(String[] args) {
	    
		System.out.println("Это Ваш личный предсказатель сегодняшней прогулки!");
		System.out.println("Введите значения температуры, скорости ветра и наличия дождя с клавиатуры через пробел или Enter: ");
		Scanner myscan = new Scanner (System.in);
		int temperature = myscan.nextInt ();
		int goldenWind = myscan.nextInt();
		String RainyDay = myscan.next();
		
		if (temperature > 35 || goldenWind > 16)
		{
		    System.out.println("Прогулка будет некомфортной или даже опасной для Вашей жизни! Воздержитесь сегодня");
		}
		else if (RainyDay.equals("Да") && (temperature > 15 && goldenWind < 8 || temperature > 20 && goldenWind < 13)  )
		{
		   System.out.println("Намечается приятная прогулка под дождем! Не простудитесь!"); 
		}
		else if (RainyDay.equals("Нет") && (temperature > 0 && goldenWind < 3 || temperature > 5 && goldenWind < 6))
		{
		    System.out.println("Звезды говорят Вам, что сегодня хороший день для прогулки!"); 
		}
		else
		{
		    System.out.println("Заварите чаю и посмотрите любимый фильм дома! Сегодня не стоит выходить на улицу"); 
		}
	}
}

