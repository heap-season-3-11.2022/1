
public class Fibonacci
{
	public static void main(String[] args) {
		System.out.println("Hello World");
		int i = 0, k = 1, count = 0, j= 1;
		do //Цикл do...while (в задании не было указано, какой из while конкретно использовать, этот компактнее)
		{
		    System.out.println(k); 
		    k = i + j;
		    i = j;
		    j = k;
		    count++;
		} while(count != 11);
		
		i = 0; k = 1; j= 1;
		for ( count = 0; count < 11; count++) //Цикл for
		{
		     System.out.println(k); 
		    k = i + j;
		    i = j;
		    j = k;
		}
	}
}
